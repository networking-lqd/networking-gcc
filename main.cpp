#include "stdafx.h"
#include "algo/OPT.h"
#include "algo/LQD.h"


template<class T, class TShort>
auto run(const std::vector<int> &test, TShort b,
         std::reference_wrapper<std::ostream> cout)
{
	T alg(b);

	return alg.exec_test(test, cout);
};


template<class TShort>
bool test_validate(const std::vector<int> &test,
                   TShort b = std::numeric_limits<TShort>::max())
{
	TShort sum = 0;

	for (auto it : test)
	{
		sum += it;

		if (sum > b)
			return false;
	}

	return sum == 0;
}


std::ostream &operator<<(std::ostream &cout, __uint128_t x)
{
	std::string curr;

	while (x > 0)
	{
		curr += char('0' + (x % 10));
		x /= 10;
	}

	std::reverse(curr.begin(), curr.end());

	cout << curr;

	return cout;
}


template<template<class, class> class F,
		template<class, class> class S, class TShort, class TLong>
double exec(const std::vector<int> &test, TShort b)
{
	assert (test_validate(test, b));

	std::ofstream fout("queues.log");

	auto num = run<F<TShort, TLong>>(test, b, fout);
//	std::cout << num << "\t(" << typeid(F<TShort, TLong>).name() << ')' << std::endl;

	auto den = run<S<TShort, TLong>>(test, b, fout);
//	std::cout << den << "\t(" << typeid(S<TShort, TLong>).name() << ')' << std::endl;

	assert (num != 0 ^ den == 0);

	return den == 0 ? 1 : num / (double) den;
}


int main()
{
	using namespace std;
	using ul = uint32_t;
	using ull = uint64_t;

	ios_base::sync_with_stdio(false);
	cin.tie(nullptr);

	const int k = 30'000, r = 736;

	static_assert(k * (long long) k < numeric_limits<ul>::max());

	const auto b = static_cast<const ul>(k * (ul) k / 3.6);

	cout << k << "\t(triangle size)" << endl;
	cout << b << "\t(buffer size)" << endl;

	vector<int> cyc = {k};
	cyc.reserve(2 * k + 1);

	for (int i = 0; i < k; i++)
	{
		cyc.push_back(-1);
		cyc.push_back(0);
	}

	vector<int> test(cyc.size() * r);

	for (int i = 0; i < r; i++)
		copy(cyc.begin(), cyc.end(), test.begin() + i * cyc.size());

	cout << test.size() << "\t(test size)" << endl;

	assert(b + test.size() < numeric_limits<uint64_t>::max());

	try
	{
#ifdef FINDB
#ifdef TERNARY
		ul lb = k * (ul) k / 4, rb = k * (ul) k / 3;

		while (lb + 30 < rb)
		{
			auto q = (2 * lb + rb) / 3, w = (lb + 2 * rb) / 3;

			if (exec<OPT, LQD, ul, ull>(test, q) <
				exec<OPT, LQD, ul, ull>(test, w))
				lb = q;
			else
				rb = w;

			cout << rb - lb << "\t(rb - lb)" << endl;
		}

		cout << k * (ul) k / (double) lb << "\t(divisor)" << endl;
#else
		const ul mult = 25;

		vector<std::future<double>> y(mult + 1);

		const int st = 3 * mult;

		for (int i = st; i < st + y.size (); i++)
		{
			double r = i / (double) mult;

			auto tb = static_cast<ul>(k * (ul) k / r);

			y[i - 3 * mult] = std::async(std::launch::deferred, exec<OPT, LQD, ul, ull>, test, tb);
		}

		cout << fixed;

		for (int i = st; i < st + y.size (); i++)
		{
			double r = i / (double) mult;

			cout << setprecision(3) << r << setprecision(10) << '\t' << y[i - 3 * mult].get() << endl;
		}
#endif
#else
		double ratio = exec<OPT, LQD, ul, ull>(test, b);

		cout << "ratio: " << setprecision(10) << fixed << ratio << endl;
#endif
	}
	catch (exception &e)
	{
		cout << e.what() << endl;
	}

	cout << "clock: " << setprecision(4) << clock() / (double) CLOCKS_PER_SEC << endl;

	return 0;
}
