// stdafx.h : include file for standard system include files,
// or project specific include files that are used frequently, but
// are changed infrequently
//

#pragma once

#include <iostream>
#include <iomanip>
#include <string>
#include <vector>
#include <queue>
#include <stack>
#include <random>
#include <algorithm>
#include <numeric>
#include <limits>
#include <map>
#include <unordered_map>
#include <set>
#include <unordered_set>
#include <memory>
#include <ostream>
#include <istream>
#include <sstream>
#include <fstream>
#include <future>
#include <typeinfo>
#include <typeindex>

#include <ctime>
#include <cassert>

