#include "../stdafx.h"
#include "device.h"


template<class TShort, class TLong>
device<TShort, TLong>::device(TShort buffer_size) :
		m_buffer_size(buffer_size), m_diff(0), m_sent(0),
		m_queue_count(0), m_filled(0)
{
}


template<class TShort, class TLong>
void device<TShort, TLong>::list_normalize()
{
	if (m_queues.empty())
		return;

	auto it = --m_queues.end();
	int cnt = 0;
	while (it != m_queues.begin() && cnt < 3)
	{
		auto prev = it;
		--prev;

		if (prev->first == it->first)
		{
			it->second += prev->second;

			m_queues.erase(prev);
		}
		else
			--it;

		cnt++;
	}

	if (!m_queues.empty() && m_queues.front().first == m_diff)
	{
		m_queue_count -= m_queues.front().second;

		m_queues.pop_front();
	}
}


template<class TShort, class TLong>
void device<TShort, TLong>::kick(TLong k)
{
	if (k == 0)
		return;

	assert(m_filled >= k);
	assert(!m_queues.empty());

	auto bound = m_diff;

	if (m_queues.size() > 1)
		bound = (++m_queues.rbegin())->first;

	auto td = std::min(k, (m_queues.back().first - bound) *
	                      (TLong) m_queues.back().second);

	m_queues.back().first -= (TShort) (td / m_queues.back().second);
	auto lin = (TShort) (td % m_queues.back().second);

	if (lin != 0)
	{
		std::pair<TShort, TShort> add = {m_queues.back().first, m_queues.back().second - lin};

		m_queues.back().first--;
		m_queues.back().second = lin;

		m_queues.push_back(add);
	}

	list_normalize();

	m_filled -= td;

	kick(k - td);
}


template<class TShort, class TLong>
TShort device<TShort, TLong>::get_queue_count() const
{
	return m_queue_count;
}


template<class TShort, class TLong>
TShort device<TShort, TLong>::size() const
{
	return m_buffer_size;
}


template<class TShort, class TLong>
void device<TShort, TLong>::resize(TShort buff_size)
{
	m_buffer_size = buff_size;

	if (m_filled > m_buffer_size)
		kick(m_filled - m_buffer_size);
}


template<class TShort, class TLong>
void device<TShort, TLong>::add_queues(TShort k)
{
	if (k == 0)
		return;

	m_filled += k * (TLong) m_buffer_size;
	m_queue_count += k;

	m_queues.emplace_back(m_buffer_size + m_diff, k);

	list_normalize();

	if (m_filled > m_buffer_size)
		kick(m_filled - m_buffer_size);
}


template<class TShort, class TLong>
void device<TShort, TLong>::del_queues(TShort k)
{
	assert(k <= m_queue_count);

	while (k > 0)
	{
		auto td = std::min(k, m_queues.back().second);

		m_queues.back().second -= td;
		m_filled -= (m_queues.back().first - m_diff) * (TLong) td;

		if (m_queues.back().second == 0)
			m_queues.pop_back();

		m_queue_count -= td;
		k -= td;
	}
}


template<class TShort, class TLong>
TShort device<TShort, TLong>::send()
{
	auto tmp = m_sent;

	m_diff++;
	m_sent += m_queue_count;
	m_filled -= m_queue_count;

	list_normalize();

	return m_sent - tmp;
}


template<class TShort, class TLong>
TShort device<TShort, TLong>::send_all()
{
	auto ans = (TShort) m_filled;

	m_queues.clear();
	m_diff = 0;
	m_queue_count = 0;
	m_filled = 0;
	m_sent += ans;

	return ans;
}


template<class TShort, class TLong>
const std::list<std::pair<TShort, TShort>> &device<TShort, TLong>::get_queues() const
{
	return m_queues;
}


template<class TShort, class TLong>
TShort device<TShort, TLong>::get_diff() const
{
	return m_diff;
}
