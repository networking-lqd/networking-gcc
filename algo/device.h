#pragma once

#include <list>
#include <vector>
#include <exception>
#include <limits>
#include <iostream>

template<class TShort, class TLong>
class device
{
	// .first stands for queue size (shifted with m_diff)
	// .second stands for number of queues of specific siZe
	// both small under assumption that m_buffer_size is in TShort
	std::list<std::pair<TShort, TShort>> m_queues;
	TShort m_diff, m_queue_count;
	TLong m_filled, m_sent;
	TShort m_buffer_size;

	void kick(TLong k);
	void list_normalize();

public:
	explicit device(TShort buffer_size);
	TShort get_queue_count() const;
	TShort size() const;
	void resize(TShort buff_size);
	void add_queues(TShort k);
	void del_queues(TShort k);
	TShort send();
	TShort send_all();
	const std::list<std::pair<TShort, TShort>> &get_queues() const;
	TShort get_diff() const;
};


template<class TShort, class TLong>
class alg
{
public:
	virtual void raise_queues(TShort k) = 0;
	virtual void kill_queues(TShort k) = 0;
	virtual TShort send() = 0;
	virtual TShort send_all() = 0;
	virtual const std::list<std::pair<TShort, TShort>> &get_queues() const = 0;
	virtual TShort get_diff() const = 0;

	TLong exec_test(const std::vector<int> &test, std::ostream &cout = std::cout)
	{
		TLong sum = 0;

		std::queue<TShort> q;

		for (auto query : test)
			if (query == 0)
			{
				q.push(get_queues().back().first - get_diff());

				auto add = send();

				if (q.size() > 3 * 30'000 * 0)
					q.pop();

				sum += add;
			}
			else if (query > 0)
				raise_queues(static_cast<TShort>(query));
			else
				kill_queues(static_cast<TShort>(-query));

		sum += send_all();

		while (!q.empty())
		{
			cout << q.front() << ' ';
			q.pop();
		}
		cout << std::endl;

		return sum;
	}
};

#include "device.ipp"
