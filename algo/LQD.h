#pragma once

#include "device.h"

#include <algorithm>

#include <cassert>


template<class TShort, class TLong>
class LQD : public alg<TShort, TLong>
{
	device<TShort, TLong> m_cd;
	TShort m_alive;

public:
	explicit LQD(TShort buffer_size = 0) : m_cd(buffer_size), m_alive(0)
	{
	}


	TShort size()
	{
		return m_cd.size();
	}


	void resize(TShort buffer_size)
	{
		m_cd.resize(buffer_size);
	}


	void raise_queues(TShort k) override
	{
		m_alive += k;

		m_cd.add_queues(k);
	}


	void kill_queues(TShort k) override
	{
		assert (k <= m_alive);

		m_alive -= k;
	}


	TShort send() override
	{
		auto ans = m_cd.send();

		m_cd.del_queues(std::min(m_alive, m_cd.get_queue_count()));
		m_cd.add_queues(m_alive);

		return ans;
	}

	TShort send_all() override
	{
		assert (m_alive == 0);

		return m_cd.send_all();
	}


	const std::list<std::pair<TShort, TShort>> &get_queues() const override
	{
		return m_cd.get_queues();
	}


	TShort get_diff() const override
	{
		return m_cd.get_diff();
	}
};
