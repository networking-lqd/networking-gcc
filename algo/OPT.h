#pragma once

#include "device.h"
#include "LQD.h"

template<class TShort, class TLong>
class OPT : public alg<TShort, TLong>
{
	TShort m_alive;
	TLong m_sent_alive;
	LQD<TShort, TLong> m_dead;

public:
	explicit OPT(TShort buff_size = 0) : m_dead(buff_size), m_alive(0), m_sent_alive(0)
	{
	}


	void raise_queues(TShort k) override
	{
		m_alive += k;

		assert(k <= m_dead.size());

		m_dead.resize(m_dead.size() - k);
	}


	void kill_queues(TShort k) override
	{
		assert(k <= m_alive);

		m_alive -= k;

		m_dead.resize(m_dead.size() + k);
		m_dead.raise_queues(k);
		m_dead.kill_queues(k);
	}

	TShort send() override
	{
		auto ans = m_dead.send() + m_alive;

		m_sent_alive += m_alive;

		return ans;
	}

	TShort send_all() override
	{
		assert(m_alive == 0);

		return m_dead.send_all();
	}


	const std::list<std::pair<TShort, TShort>> &get_queues() const override
	{
		return m_dead.get_queues();
	}


	TShort get_diff() const override
	{
		return m_dead.get_diff();
	}
};
